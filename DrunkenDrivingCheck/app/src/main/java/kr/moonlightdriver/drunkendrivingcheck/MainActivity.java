package kr.moonlightdriver.drunkendrivingcheck;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {
    private static final String[] mRepeatTimeList = new String[]{ "5", "10", "15", "20", "30", "60" };

    private static final String mSite_1_URL = "https://du3.shjey.com/api_v19/initMap.php";      // 더더더
    private static final String mSite_1_comment_URL = "https://du3.shjey.com/api_v19/getComments.php";      // 더더더
    private static final String mSite_2_URL = "";       // 삐뽀삐뽀
    private static final String mSite_3_URL = "http://ws.apptube.co.kr/driving/drunkenPointPastList";       // 피하세

    private CheckBox mCheckBoxSite_1;
    private CheckBox mCheckBoxSite_2;
    private CheckBox mCheckBoxSite_3;
    private CheckBox mCheckBoxRepeat;
    private Spinner mSpinnerRepeatTime;
    private Button mBtnExecute;
    private ListView mListViewResult;
    private ArrayAdapter<String> mAdapterResult;

    private ArrayList<String> mResultList;

    private boolean mIsExecuting;

    private int mSelectedRepeatTime;

    private int mExecutionCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mResultList = new ArrayList<>();
        mIsExecuting = false;
        mSelectedRepeatTime = 0;
        mExecutionCount = 0;

        mListViewResult = (ListView) findViewById(R.id.result_list);
        mAdapterResult = new ArrayAdapter<>(MainActivity.this, R.layout.support_simple_spinner_dropdown_item, mResultList);
        mListViewResult.setAdapter(mAdapterResult);

        mCheckBoxSite_1 = (CheckBox) findViewById(R.id.site_1);
        if (mCheckBoxSite_1 != null) {
            mCheckBoxSite_1.setChecked(true);
        }
        mCheckBoxSite_2 = (CheckBox) findViewById(R.id.site_2);
        if (mCheckBoxSite_2 != null) {
            mCheckBoxSite_2.setChecked(false);
        }
        mCheckBoxSite_3 = (CheckBox) findViewById(R.id.site_3);
        if (mCheckBoxSite_3 != null) {
            mCheckBoxSite_3.setChecked(false);
        }

        mCheckBoxRepeat = (CheckBox) findViewById(R.id.repeat);
        mSpinnerRepeatTime = (Spinner) findViewById(R.id.repeat_time);
        mBtnExecute = (Button) findViewById(R.id.btnExecute);

        mBtnExecute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickExecute();
            }
        });

        mCheckBoxRepeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCheckBoxRepeat.isChecked()) {
                    mSpinnerRepeatTime.setEnabled(true);
                    ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_spinner_dropdown_item, mRepeatTimeList);
                    mSpinnerRepeatTime.setAdapter(spinnerAdapter);
                } else {
                    mSpinnerRepeatTime.setEnabled(false);
                    mSpinnerRepeatTime.setAdapter(null);
                }
            }
        });

        mSpinnerRepeatTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mSelectedRepeatTime = Integer.parseInt(mRepeatTimeList[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        mSpinnerRepeatTime.setEnabled(false);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private void setExecuteStatus() {
        mIsExecuting = !mIsExecuting;

        if(mIsExecuting) {
            mBtnExecute.setEnabled(false);
            mBtnExecute.setText("조회중...");
        } else {
            mBtnExecute.setEnabled(true);
            mBtnExecute.setText("실행");
        }
    }

    private void onClickExecute() {
        if(mIsExecuting) {
            return;
        }

        if(mCheckBoxRepeat.isChecked() && mSelectedRepeatTime <= 0) {
            Toast.makeText(MainActivity.this, "반복 실행 시간을 선택해주세요.", Toast.LENGTH_SHORT).show();
            return;
        }

        setExecuteStatus();

        mResultList.clear();
        mAdapterResult.notifyDataSetChanged();

        if(mCheckBoxSite_1.isChecked()) {
            mExecutionCount++;
            getSite1Data(mCheckBoxSite_1.getText().toString());
        }

        if (mCheckBoxSite_2.isChecked()) {
            mExecutionCount++;
//            getSite2Data(mCheckBoxSite_2.getText().toString());
        }

        if(mCheckBoxSite_3.isChecked()) {
            mExecutionCount++;
            getSite3Data(mCheckBoxSite_3.getText().toString());
        }
    }

    private void updateResultList(String _siteName, String _result) {
        int listSize = mResultList.size();
        for (int i = 0; i < listSize; i++) {
            if(mResultList.get(i).startsWith(_siteName)) {
                mResultList.set(i, _siteName + _result);
                mExecutionCount--;
                break;
            }
        }

        mAdapterResult.notifyDataSetChanged();

        if(mExecutionCount == 0) {
            setExecuteStatus();
        }
    }

    private void sendDataToServer(final String _siteName, String _ret_params, final int _ret_count) {
        try {
            updateResultList(_siteName, " 서버 업로드 중...");

            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            RequestParams post_params = new RequestParams();
            post_params.add("type", "update_crackdown_data");
            post_params.add("crackdown_data", _ret_params);

            asyncHttpClient.post("http://115.68.122.108/ajax/crackdown.php", post_params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    try {
                        String response = new String(responseBody);
                        JSONObject ret = new JSONObject(response);
                        if(ret.getString("result").equals("OK")) {
                            updateResultList(_siteName, " 완료 [성공] - " + _ret_count + "건");
                        } else {
                            updateResultList(_siteName, " 완료 [실패]");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        updateResultList(_siteName, " 완료 [실패]");
                    }
                }

                @Override
                public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                    error.printStackTrace();
                    updateResultList(_siteName, " 완료 [실패]");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getSite1Data(final String _siteName) {
        mResultList.add(_siteName + " 조회중...");

        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        asyncHttpClient.get(mSite_1_URL, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String response = new String(responseBody);
                    if(response.isEmpty()) {
                        updateResultList(_siteName, " 완료 [실패]");
                        return;
                    }

                    JSONObject responseObj = new JSONObject(response);
                    JSONArray crackdowns = responseObj.optJSONArray("crackdowns");
                    if(crackdowns == null || crackdowns.length() <= 0) {
                        updateResultList(_siteName, " 완료 [실패]");
                        return;
                    }

                    JSONArray ret_params = new JSONArray();
                    int listSize = crackdowns.length();
                    for (int i = 0; i < listSize; i++) {
                        JSONObject ret = crackdowns.getJSONObject(i);
                        JSONObject params = new JSONObject();
                        params.put("seq", ret.getLong("seq"));
                        params.put("sido", ret.getString("sido"));
                        params.put("sigugun", ret.getString("sigugun"));
                        params.put("address", ret.getString("address"));
                        params.put("category", ret.getString("category"));

                        JSONObject locations = new JSONObject();
                        JSONArray coordinates = new JSONArray();
                        coordinates.put(ret.getDouble("longitude"));
                        coordinates.put(ret.getDouble("latitude"));
                        locations.put("type", "Point");
                        locations.put("coordinates", coordinates);
                        params.put("locations", locations);

                        params.put("likeCount", 0);
                        params.put("dislikeCount", 0);
                        params.put("commentCount", 0);
                        params.put("createTime", ret.getLong("createTime"));

                        params.put("from", "더더더");

                        ret_params.put(params);

                        if(ret.getInt("commentCount") > 0 && ret.getString("category").toLowerCase().equals("police")) {
//                            getComment(ret.getLong("seq"));
                        }
                    }

                    sendDataToServer(_siteName, ret_params.toString(), ret_params.length());
                } catch (Exception e) {
                    e.printStackTrace();
                    updateResultList(_siteName, " 완료 [실패]");
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                updateResultList(_siteName, " 완료 [실패]");
            }
        });
    }

    private void getComment(long _seq) {
        try {
            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            RequestParams parameters = new RequestParams();
            parameters.put("markerType", "crackdown");
            parameters.put("markerSeq", _seq);
            parameters.put("requestCode", 0);

            asyncHttpClient.post(mSite_1_comment_URL, parameters, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    try {
                        String response = new String(responseBody);
                        Log.e("getComment", response);
                        if(response.isEmpty()) {

                            return;
                        }

//                        JSONObject responseObj = new JSONObject(response);
//                        JSONArray crackdowns = responseObj.optJSONArray("crackdowns");
//                        if(crackdowns == null || crackdowns.length() <= 0) {
//                            updateResultList(_siteName, " 완료 [실패]");
//                            return;
//                        }
//
//                        JSONArray ret_params = new JSONArray();
//                        int listSize = crackdowns.length();
//                        for (int i = 0; i < listSize; i++) {
//                            JSONObject ret = crackdowns.getJSONObject(i);
//                            JSONObject params = new JSONObject();
//                            params.put("seq", ret.getLong("seq"));
//                            params.put("sido", ret.getString("sido"));
//                            params.put("sigugun", ret.getString("sigugun"));
//                            params.put("address", ret.getString("address"));
//                            params.put("category", ret.getString("category"));
//
//                            JSONObject locations = new JSONObject();
//                            JSONArray coordinates = new JSONArray();
//                            coordinates.put(ret.getDouble("longitude"));
//                            coordinates.put(ret.getDouble("latitude"));
//                            locations.put("type", "Point");
//                            locations.put("coordinates", coordinates);
//                            params.put("locations", locations);
//
//                            params.put("likeCount", ret.getInt("likeCount"));
//                            params.put("dislikeCount", ret.getInt("dislikeCount"));
//                            params.put("commentCount", ret.getInt("commentCount"));
//                            params.put("createTime", ret.getLong("createTime"));
//
//                            params.put("from", "더더더");
//
//                            ret_params.put(params);
//
//                            if(ret.getInt("commentCount") > 0) {
//                                getComment(ret.getLong("seq"));
//                            }
//                        }
//
//                        sendDataToServer(_siteName, ret_params.toString(), ret_params.length());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                    error.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getSite2Data(final String _siteName) {
        mResultList.add(_siteName + " 조회중...");

        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        asyncHttpClient.get(mSite_2_URL, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String response = new String(responseBody);
                    if(response.isEmpty()) {
                        updateResultList(_siteName, " 완료 [실패]");
                        return;
                    }

                    JSONObject responseObj = new JSONObject(response);
                    JSONArray crackdowns = responseObj.optJSONArray("crackdowns");
                    if(crackdowns == null || crackdowns.length() <= 0) {
                        updateResultList(_siteName, " 완료 [실패]");
                        return;
                    }

                    JSONArray ret_params = new JSONArray();
                    int listSize = crackdowns.length();
                    for (int i = 0; i < listSize; i++) {
                        JSONObject ret = crackdowns.getJSONObject(i);
                        JSONObject params = new JSONObject();
                        params.put("sido", ret.getString("sido"));
                        params.put("sigugun", ret.getString("sigugun"));
                        params.put("address", ret.getString("address"));
                        params.put("category", ret.getString("category"));

                        JSONObject locations = new JSONObject();
                        JSONArray coordinates = new JSONArray();
                        coordinates.put(ret.getDouble("longitude"));
                        coordinates.put(ret.getDouble("latitude"));
                        locations.put("type", "Point");
                        locations.put("coordinates", coordinates);
                        params.put("locations", locations);

                        params.put("likeCount", ret.getString("likeCount"));
                        params.put("dislikeCount", ret.getString("dislikeCount"));
                        params.put("createTime", ret.getString("createTime"));

                        ret_params.put(params);
                    }

                    sendDataToServer(_siteName, ret_params.toString(), ret_params.length());
                } catch (Exception e) {
                    e.printStackTrace();
                    updateResultList(_siteName, " 완료 [실패]");
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                updateResultList(_siteName, " 완료 [실패]");
            }
        });
    }

    private void getSite3Data(final String _siteName) {
        mResultList.add(_siteName + " 조회중...");

        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        asyncHttpClient.get(mSite_3_URL, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String response = new String(responseBody);
                    if(response.isEmpty()) {
                        updateResultList(_siteName, " 완료 [실패]");
                        return;
                    }

                    JSONObject responseObj = new JSONObject(response);
                    JSONArray crackdowns = responseObj.optJSONArray("itemArray");
                    if(crackdowns == null || crackdowns.length() <= 0) {
                        updateResultList(_siteName, " 완료 [실패]");
                        return;
                    }

                    JSONArray ret_params = new JSONArray();
                    int listSize = crackdowns.length();
                    for (int i = 0; i < listSize; i++) {
                        JSONObject ret = crackdowns.getJSONObject(i);
                        JSONObject params = new JSONObject();

                        String address = ret.getString("address");
                        if(!address.isEmpty()) {
                            String[] addressSplit = address.split(" ");
                            params.put("sido", addressSplit[0]);
                            params.put("sigugun", addressSplit[1]);
                            params.put("address", address.replace(addressSplit[0] + " ", "").replace(addressSplit[1] + " ", ""));
                        }

                        params.put("created", ret.getString("created"));
                        JSONObject locations = new JSONObject();
                        JSONArray coordinates = new JSONArray();
                        coordinates.put(ret.getDouble("lng"));
                        coordinates.put(ret.getDouble("lat"));
                        locations.put("type", "Point");
                        locations.put("coordinates", coordinates);
                        params.put("locations", locations);
                        params.put("from", "피하세");

                        ret_params.put(params);
                    }

                    sendDataToServer(_siteName, ret_params.toString(), ret_params.length());
                } catch (Exception e) {
                    e.printStackTrace();
                    updateResultList(_siteName, " 완료 [실패]");
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                updateResultList(_siteName, " 완료 [실패]");
            }
        });
    }
}
